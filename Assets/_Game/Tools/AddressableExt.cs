using System;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.UI;

namespace _Game.Tools
{
	public static class AddressableExt
	{
		// This function is using for async loading of Addressable assets by AssetReference
		public static AsyncOperationHandle<TObject> LoadAssetAsyncIfValid<TObject>(this AssetReference assetReference, Action<AsyncOperationHandle<TObject>> OnLoaded = null)
		{
			AsyncOperationHandle op = assetReference.OperationHandle;
			AsyncOperationHandle<TObject> handle = default(AsyncOperationHandle<TObject>);
 
			if (assetReference.IsValid() && op.IsValid())
			{
				// Increase the usage counter & Convert.
				Addressables.ResourceManager.Acquire(op);
				handle = op.Convert<TObject>();
 
				if (handle.IsDone)
				{
					OnLoaded(handle);
				}
				else
				{
					// Removed OnLoaded in-case it's already been added.
					handle.Completed -= OnLoaded;
					handle.Completed += OnLoaded;
				}
			}
			else
			{
				handle = assetReference.LoadAssetAsync<TObject>();
 
				// Removed OnLoaded in-case it's already been added.
				handle.Completed -= OnLoaded;
				handle.Completed += OnLoaded;
			}
 
			return handle;
		}

		public static void LoadToImage(this AssetReferenceSprite reference, Image targetImage)
		{
			reference.LoadAssetAsyncIfValid<Sprite>(handle =>
			{
				targetImage.sprite = handle.Result;
			});
		}

		public static void LoadSprite(this AssetReferenceSprite reference, Action<Sprite> onLoaded)
		{
			reference.LoadAssetAsyncIfValid<Sprite>(handle =>
			{
				onLoaded?.Invoke(handle.Result);
			});
		}
	}
}