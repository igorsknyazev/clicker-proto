using System;
using UnityEngine;

namespace _Game.Tools
{
	[RequireComponent(typeof(CanvasGroup))]
	public class UIHideableCanvasGroup : MonoBehaviour
	{
		public bool visibleOnStart;

		private CanvasGroup _canvasGroup;
		public CanvasGroup CGroup => _canvasGroup ??= GetComponent<CanvasGroup>();

		private void Awake()
		{
			SetVisible(visibleOnStart);
		}

		public void SetVisible(bool isVisible)
		{
			CGroup.alpha = isVisible ? 1 : 0;
			CGroup.interactable = isVisible;
			CGroup.blocksRaycasts = isVisible;
		}
	}
}