using System;
using UnityEngine;

namespace _Game.Tools
{
	public class BaseSingletonBehaviour<T> : MonoBehaviour where T : MonoBehaviour
	{
		private static T _instance;
		public static T instance => _instance;

		protected virtual void OnDestroy()
		{
			_instance = null;
		}

		protected virtual void Awake()
		{
			if (_instance != null)
			{
				Debug.LogWarning($"Instance of {GetType().FullName} already exists. It will be overwritten!");
			}

			_instance = gameObject.GetComponent<T>();
		}
	}
}