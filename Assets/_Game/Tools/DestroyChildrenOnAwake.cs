using System;
using UnityEngine;

namespace _Game.Tools
{
	// This class destroys all demo children of transform
	public class DestroyChildrenOnAwake : MonoBehaviour
	{
		private void Awake()
		{
			DestroyChildren();
		}

		public void DestroyChildren()
		{
			foreach (Transform child in transform)
			{
				Destroy(child.gameObject);
			}
		}
	}
}