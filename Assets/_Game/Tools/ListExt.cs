using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace _Game.Tools
{
	public static class ListExt
	{
		public static List<string> ToList(this TextAsset ta) {
			return new List<string>(ta.text.Split('\n'));
		}
		
		public static T GetRandomElement<T>(this List<T> list)
		{
			return list[UnityEngine.Random.Range(0, list.Count)];
		}

		public static int MaxOrDefault<T>(this List<T> list, Func<T, int> selector)
		{
			return list.Any() ? list.Max(selector) : default(int);
		}
	}
}