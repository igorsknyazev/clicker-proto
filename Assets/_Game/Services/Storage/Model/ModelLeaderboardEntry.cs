using System;

namespace _Game.Services.Storage.Model
{
	[Serializable]
	public class ModelLeaderboardEntry
	{
		public int id { get; set; }
		public string name { get; set; }
		public float time { get; set; }
	}
}