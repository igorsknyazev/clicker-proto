using System;
using System.Collections.Generic;
using System.Linq;
using _Game.Data;
using _Game.Tools;

namespace _Game.Services.Storage.Model
{
	[Serializable]
	public class ModelLevelsProgress
	{
		public Dictionary<string, ModelLevel> levelsProgress { get; set; }

		public void AddLevelResult(Level level, float timeResult)
		{
			List<ModelLeaderboardEntry> leaderboardEntries = new List<ModelLeaderboardEntry>();
			ModelLevel savedLevelData;
			// level.name - is an Asset name! Not readable name!
			if (levelsProgress.TryGetValue(level.name, out savedLevelData))
			{
				leaderboardEntries = savedLevelData.leaderboard;
			}
			else
			{
				levelsProgress.Add(level.name, new ModelLevel()
				{
					leaderboard = leaderboardEntries,
					stars = 0,
				});
			}

			try
			{
				// Update previous player result
				// Find player by name, because IDs aren't usable and weren't described in Technical Task
				var previousResult = leaderboardEntries.First(entry => entry.name == StorageService.instance.Settings.PlayerName);

				if (previousResult.time > timeResult)
				{
					previousResult.time = timeResult;
				}
			}
			catch
			{
				// Previous result for player isn't found. So create new one
				
				leaderboardEntries.Add(new ModelLeaderboardEntry()
				{
					id = leaderboardEntries.MaxOrDefault(entry => entry.id) + 1,
					name = StorageService.instance.Settings.PlayerName,
					time = timeResult,
				});
			}
		}
	}
}