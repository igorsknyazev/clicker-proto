using System;

namespace _Game.Services.Storage.Model
{
	[Serializable]
	public class ModelSettings
	{
		[NonSerialized]
		public Action<bool> OnSoundChanged;
		[NonSerialized]
		public Action<bool> OnMusicChanged;
		[NonSerialized]
		public Action<string> OnPlayerNameChanged;

		private bool _hasSound;
		public bool HasSound
		{
			get => _hasSound;
			set
			{
				_hasSound = value;
				OnSoundChanged?.Invoke(_hasSound);
			}
		}

		private bool _hasMusic;

		public bool HasMusic
		{
			get => _hasMusic;
			set
			{
				_hasMusic = value;
				OnMusicChanged?.Invoke(_hasMusic);
			}
		}

		private string _playerName;

		public string PlayerName
		{
			get => _playerName;
			set
			{
				_playerName = value;
				OnPlayerNameChanged?.Invoke(_playerName);
			}
		}
	}
}