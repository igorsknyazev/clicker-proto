using System;
using System.Collections.Generic;

namespace _Game.Services.Storage.Model
{
	[Serializable]
	public class ModelLevel
	{
		public int stars { get; set; }
		public List<ModelLeaderboardEntry> leaderboard { get; set; }
	}
}