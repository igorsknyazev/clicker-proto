using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using _Game.Services.Storage.Model;
using _Game.Tools;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace _Game.Services.Storage
{
	public class StorageService : BaseSingletonBehaviour<StorageService>
	{
		#region save data fields
		private ModelLevelsProgress _progress;

		public ModelLevelsProgress Progress
		{
			get => _progress;
			set => _progress = value;
		}

		private ModelSettings _settings;

		public ModelSettings Settings
		{
			get => _settings;
			set => _settings = value;
		}
		#endregion

		protected override void Awake()
		{
			base.Awake();
			DontDestroyOnLoad(this);
		}

		private void Start()
		{
			// It depends on AssetsService (loads right after all assets will load)
			AssetsService.instance.OnPreloadComplete += LoadData;
		}

		private void LoadData()
		{
			Progress = LoadModel(Config.GAME_FILE_SAVE_PROGRESS, ModelDefaultInitializer.InitProgress);
			Settings = LoadModel(Config.GAME_FILE_SAVE_SETTINGS, ModelDefaultInitializer.InitSettings);
			StartGame();
		}
		
		private void StartGame()
		{
			SceneManager.LoadScene("_Game");
		}

		private void OnDisable()
		{
			SaveData();
		}

		private void OnApplicationFocus(bool hasFocus)
		{
			if (!hasFocus)
			{
				SaveData();
			}
		}

		private void OnApplicationPause(bool pauseStatus)
		{
			SaveData();
		}

		#region Serialization to files

		private TModel LoadModel<TModel>(string saveFileName, Func<TModel> defaultInitializer)
		{
			try
			{
				var path = Path.Combine(Config.SAVE_FOLDER, saveFileName);
				
				if (!File.Exists(path))
				{
					throw new Exception($"Save file {saveFileName} doesn't exist. Init with default data");
				}
				
				// Save file exists
				var file = File.Open(path, FileMode.Open);
				var bf = new BinaryFormatter();
				var gameData = (TModel) bf.Deserialize(file);
				file.Close();

				return gameData;
			}
			catch (Exception e)
			{
				Debug.Log(e);
				return defaultInitializer();
			}
		}

		private void SaveData()
		{
			WriteModel(Progress, Config.GAME_FILE_SAVE_PROGRESS);
			WriteModel(Settings, Config.GAME_FILE_SAVE_SETTINGS);
		}

		private void WriteModel<TModel>(TModel targetModel, string saveFileName)
		{
			CreateSaveFolderIfNotExists();

			var path = Path.Combine(Config.SAVE_FOLDER, saveFileName);

			var bf = new BinaryFormatter();
			var file = File.Create(path);
			bf.Serialize(file, targetModel);
			file.Close();
			
			Debug.Log($"Model {typeof(TModel)} was saved to {path}");
		}
		
		public static void CreateSaveFolderIfNotExists()
		{
			if (!Directory.Exists(Config.SAVE_FOLDER))
			{
				Debug.Log("Save directory does not exist. New one will be created");
				Directory.CreateDirectory(Config.SAVE_FOLDER);
			}
		}

		#endregion
	}
}