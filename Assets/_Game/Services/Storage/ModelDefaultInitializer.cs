using System.Collections.Generic;
using _Game.Services.Storage.Model;
using _Game.Tools;

namespace _Game.Services.Storage
{
	public static class ModelDefaultInitializer
	{
		public static ModelLevelsProgress InitProgress()
		{
			return new ModelLevelsProgress()
			{
				levelsProgress = AssetsService.instance.remoteDemoData,
			};
		}

		public static ModelSettings InitSettings()
		{
			return new ModelSettings()
			{
				HasMusic = true,
				HasSound = true,
				PlayerName = AssetsService.instance.playerNames.GetRandomElement(),
			};
		}
	}
}