using System;
using _Game.Services.Storage;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace _Game.Services
{
	public class PreloadDataChecker : MonoBehaviour
	{
		private void Awake()
		{
			if (AssetsService.instance == null || StorageService.instance == null)
			{
				SceneManager.LoadScene("_Initiator");
			}
		}
	}
}