using System;
using System.Collections;
using System.Collections.Generic;
using _Game.Data;
using _Game.Services.Storage.Model;
using _Game.Tools;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.Networking;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace _Game.Services
{
	public class AssetsService : BaseSingletonBehaviour<AssetsService>
	{
		public Action OnPreloadComplete;
		
		public Dictionary<string, Level> levels = new Dictionary<string, Level>();

		public List<string> playerNames = new List<string>();
		public Dictionary<string, ModelLevel> remoteDemoData = new Dictionary<string, ModelLevel>();

		protected override void Awake()
		{
			base.Awake();
			DontDestroyOnLoad(this);
		}

		private int _routinesRunning;

		private IEnumerator Start()
		{
			_routinesRunning = 2;
			StartCoroutine(PreloadAssets());
			StartCoroutine(DownloadDemoData());

			while (_routinesRunning > 0)
			{
				yield return null;
			}

			OnPreloadComplete?.Invoke();
		}

		private IEnumerator PreloadAssets()
		{
			// Skip a frame to allow all OnPreloadComplete subscribers to sub
			yield return new WaitForEndOfFrame();
			
			// Load levels
			AsyncOperationHandle<IList<Level>> handleLevels = Addressables.LoadAssetsAsync<Level>("Levels", null);
			yield return handleLevels;

			// Store levels in memory reference
			foreach (var levelScriptable in handleLevels.Result)
			{
				levels[levelScriptable.name] = levelScriptable;
			}
			
			// Load available player names
			AsyncOperationHandle<TextAsset> handleNames =
				Addressables.LoadAssetAsync<TextAsset>("Assets/_Game/Data/playerNames.txt");
			yield return handleNames;

			playerNames = handleNames.Result.ToList();

			_routinesRunning--;
		}

		private IEnumerator DownloadDemoData()
		{
			using UnityWebRequest webRequest = UnityWebRequest.Get(Config.DEMO_DATA_URI);
			webRequest.timeout = 3;
			yield return webRequest.SendWebRequest();

			if (webRequest.result != UnityWebRequest.Result.Success)
			{
				Debug.Log($"Cannot receive demo data from remote with error: {webRequest.error}");
				_routinesRunning--;
				yield break;
			}
			
			// trying to deserialize received data
			try
			{
				remoteDemoData =
					JsonConvert.DeserializeObject<Dictionary<string, ModelLevel>>(webRequest.downloadHandler.text);
			}
			catch (Exception e)
			{
				Debug.Log($"Error when deserializing remote data: {e}");
			}
			finally
			{
				_routinesRunning--;
			}
		}
	}
}