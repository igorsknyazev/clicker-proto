using System;
using UnityEngine;
using UnityEngine.UI;

namespace _Game.Services.Sound
{
	[RequireComponent(typeof(Button))]
	public class ButtonPlaySoundOnClick : MonoBehaviour
	{
		private void Awake()
		{
			GetComponent<Button>().onClick.AddListener(PlaySound);
		}

		private void PlaySound()
		{
			SoundService.instance.PlayButtonSound();
		}
	}
}