using System;
using _Game.Services.Storage;
using _Game.Tools;
using Lean.Pool;
using UnityEngine;
using UnityEngine.Audio;

namespace _Game.Services.Sound
{
	public class SoundService : BaseSingletonBehaviour<SoundService>
	{
		public AudioSource audioSourceMusic;
		public AudioMixer audioMixer;

		public AudioSource prefabSoundClick;
		private void Start()
		{
			StorageService.instance.Settings.OnMusicChanged += UpdateMusic;
			StorageService.instance.Settings.OnSoundChanged += UpdateSound;
			UpdateMusic(StorageService.instance.Settings.HasMusic);
			UpdateSound(StorageService.instance.Settings.HasSound);
		}

		private void UpdateMusic(bool hasMusic)
		{
			if (audioSourceMusic.isPlaying && !hasMusic)
			{
				audioSourceMusic.Pause();
			} else if (!audioSourceMusic.isPlaying && hasMusic)
			{
				audioSourceMusic.Play();
			}
			
			SetMixerValue("VolumeMusic", hasMusic);
		}

		private void UpdateSound(bool hasSound)
		{
			SetMixerValue("VolumeSound", hasSound);
		}
		
		private void SetMixerValue(string paramName, bool enabled)
		{
			audioMixer.SetFloat(paramName, enabled ? 0 : -80);
		}

		public void PlayButtonSound()
		{
			if (!StorageService.instance.Settings.HasSound)
			{
				return;
			}

			var soundObj = LeanPool.Spawn(prefabSoundClick);
			// Sound sometimes stop playing if despawn too early
			LeanPool.Despawn(soundObj, soundObj.clip.length + 1f);
		}
	}
}