using _Game.UI.Popups.Popup_Settings;
using UnityEngine;

namespace _Game.UI.FloatingControls
{
	public class ButtonOpenSettings : MonoBehaviour
	{
		public void OpenSettingsPopup()
		{
			PopupSettings.instance.Show();
		}
	}
}