using _Game.Data;
using _Game.Services.Storage;
using _Game.Services.Storage.Model;
using UnityEngine;
using UnityEngine.UI;

namespace _Game.UI.Common.StarsRating
{
	public class StarsRating : MonoBehaviour
	{
		public Image[] imagesStar;
		public Color colorActiveStar = Color.yellow;
		public Color colorEmptyStar = Color.gray;
		
		public void Init(Level level)
		{
			ModelLevel savedLevelData;
			// level.name - is an Asset name! Not readable name!
			if (StorageService.instance.Progress.levelsProgress.TryGetValue(level.name, out savedLevelData))
			{
				ColorStars(savedLevelData.stars);
			}
			else
			{
				ColorStars(0);
			}
		}

		private void ColorStars(int stars)
		{
			if (stars > imagesStar.Length)
			{
				// Color all stars as active
				foreach (var image in imagesStar)
				{
					image.color = colorActiveStar;
				}
			}
			else
			{
				for (int i = 0; i < stars; i++)
				{
					imagesStar[i].color = colorActiveStar;
				}

				for (int i = stars; i < imagesStar.Length; i++)
				{
					imagesStar[i].color = colorEmptyStar;
				}
			}
		}
	}
}