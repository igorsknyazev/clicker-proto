using _Game.Tools;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.UI;

namespace _Game.UI.Pages.Page_Game
{
	[RequireComponent(typeof(Image))]
	[RequireComponent(typeof(AspectRatioFitter))]
	public class ImageLevelBackground : MonoBehaviour
	{
		private Image _image;
		private Image image => _image ??= GetComponent<Image>();
		private AspectRatioFitter _aspectRatioFitter;
		private AspectRatioFitter aspectRatioFitter => _aspectRatioFitter ??= GetComponent<AspectRatioFitter>();
		
		public void Init(AssetReferenceSprite refSprite)
		{
			refSprite.LoadSprite(sprite =>
			{
				image.sprite = sprite;
				var ratio = sprite.rect.width / sprite.rect.height;
				aspectRatioFitter.aspectRatio = ratio;
			});
		}
	}
}