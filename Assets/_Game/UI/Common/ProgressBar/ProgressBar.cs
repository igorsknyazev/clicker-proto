using UnityEngine;
using UnityEngine.UI;

namespace _Game.UI.ProgressBar
{
	public class ProgressBar : MonoBehaviour
	{
		public Image imageFill;
		
		public void SetValue(float val)
		{
			imageFill.fillAmount = Mathf.Clamp(val, 0, 1);
		}
	}
}