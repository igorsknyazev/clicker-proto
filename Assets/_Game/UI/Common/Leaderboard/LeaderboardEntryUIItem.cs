using _Game.Services.Storage.Model;
using TMPro;
using UnityEngine;

namespace _Game.UI.Common.Leaderboard
{
	public class LeaderboardEntryUIItem : MonoBehaviour
	{
		public TextMeshProUGUI textPlayerName;
		public TextMeshProUGUI textTimeSpent;
		
		public void Init(ModelLeaderboardEntry modelLeaderboard)
		{
			textPlayerName.text = modelLeaderboard.name;
			textTimeSpent.text = $"{modelLeaderboard.time:N1}";
		}
	}
}