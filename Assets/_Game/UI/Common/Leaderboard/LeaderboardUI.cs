using System.Collections.Generic;
using System.Linq;
using _Game.Data;
using _Game.Services.Storage;
using _Game.Services.Storage.Model;
using _Game.Tools;
using _Game.UI.Popups.Popup_LevelInfo;
using UnityEngine;

namespace _Game.UI.Common.Leaderboard
{
	public class LeaderboardUI : MonoBehaviour
	{
		[Header("Leaderboard")]
		public RectTransform boxRating;
		public LeaderboardEntryUIItem prefabLeaderboardEntry;
		public GameObject prefabNoLeaders;
		public DestroyChildrenOnAwake childrenDestroyer;
		
		public void Init(Level level)
		{
			childrenDestroyer.DestroyChildren();

			List<ModelLeaderboardEntry> leaderboardEntries = new List<ModelLeaderboardEntry>();
			ModelLevel savedLevelData;
			// level.name - is an Asset name! Not readable name!
			if (StorageService.instance.Progress.levelsProgress.TryGetValue(level.name, out savedLevelData))
			{
				leaderboardEntries = savedLevelData.leaderboard;
			}

			if (leaderboardEntries.Count > 0)
			{
				foreach (var entry in leaderboardEntries.OrderBy(entry => entry.time))
				{
					var leaderboardUIItem = Instantiate(prefabLeaderboardEntry, boxRating);
					leaderboardUIItem.Init(entry);
				}
			}
			else
			{
				Instantiate(prefabNoLeaders, boxRating);
			}
		}
	}
}