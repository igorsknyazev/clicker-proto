using System;
using System.Collections.Generic;
using _Game.Tools;

namespace _Game.UI.Pages
{
	public class PageNavigator : BaseSingletonBehaviour<PageNavigator>
	{
		public Page pageLevels;
		public Page pageGame;
		
		private Stack<Page> _pages = new Stack<Page>();

		private void Start()
		{
			Push(pageLevels);
		}

		public void Push(Page targetPage)
		{
			if (_pages.Count >= 1)
			{
				var topPage = _pages.Peek();
				topPage.DisablePage();
			}
			
			_pages.Push(targetPage);
			targetPage.EnablePage();
		}

		public void Pop()
		{
			if (_pages.Count > 1)
			{
				var topPage = _pages.Pop();
				topPage.DisablePage();
			}
			
			// Activate last top page
			var targetPage = _pages.Peek();
			targetPage.EnablePage();
		}
	}
}