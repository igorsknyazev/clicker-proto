using System.Collections;
using _Game.Data;
using _Game.Services.Storage;
using _Game.Tools;
using _Game.UI.Pages.Page_Game.Bonuses;
using _Game.UI.Popups.Popup_FinishedLevel;
using TMPro;
using UnityEngine;

namespace _Game.UI.Pages.Page_Game
{
	public class PageGame : BaseSingletonBehaviour<PageGame>
	{
		public int amountToCompleteLevel = 15;
		
		[Header("UI components")]
		public ImageLevelBackground imageLevelBackground;
		public ProgressBar.ProgressBar progressBar;
		public TextMeshProUGUI textTimer;
		public TextMeshProUGUI textProgress;

		[Header("Game target")] public HuntObject huntObject;
		
		private Level _level;
		private int _progress;
		
		public void StartGame(Level level)
		{
			_level = level;

			imageLevelBackground.Init(_level.backgroundImage);
			huntObject.Init(_level.targetImage);

			_progress = 0;
			RedrawProgress(_progress);
			
			StartCoroutine(TimerCoroutine());
			BonusManager.instance.StartBonusSpawn();
		}

		private void RedrawProgress(int progress)
		{
			progressBar.SetValue((float) progress / amountToCompleteLevel);
			textProgress.text = $"{progress} / {amountToCompleteLevel}";
		}

		private void RedrawTimeGone(float timeGone)
		{
			textTimer.text = $"{timeGone:N1}";
		}

		private void StopGame()
		{
			StopAllCoroutines();
			BonusManager.instance.StopBonusSpawn();
		}

		public void ClosePage()
		{
			StopGame();
			PageNavigator.instance.Pop();
		}

		private float _timeGone;

		private IEnumerator TimerCoroutine()
		{
			_timeGone = 0;

			while (true)
			{
				RedrawTimeGone(_timeGone);
				_timeGone += Time.deltaTime;
				yield return null;
			}
		}

		public void AddProgress(int amount)
		{
			_progress += amount;
			RedrawProgress(_progress);

			if (_progress >= amountToCompleteLevel)
			{
				// Finish level!
				StopGame();
				StorageService.instance.Progress.AddLevelResult(_level, _timeGone);
				PopupFinishedLevel.instance.Show(_level, _timeGone);
			}
		}
	}
}