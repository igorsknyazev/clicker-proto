using System;
using _Game.Tools;
using _Game.UI.Pages.Page_Game.Bonuses;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace _Game.UI.Pages.Page_Game
{
	public class HuntObject : MonoBehaviour
	{
		public float bonusScale = 1.5f;
		
		public Image icon;

		private void Start()
		{
			BonusManager.instance.OnBonusActivate += bonusType =>
			{
				if (bonusType == BonusType.BiggerHuntObject)
				{
					SetScale(bonusScale);
				}
			};
			
			BonusManager.instance.OnBonusFinished += bonusType =>
			{
				if (bonusType == BonusType.BiggerHuntObject)
				{
					SetScale(1);
				}
			};
		}

		public void Init(AssetReferenceSprite spriteRef)
		{
			spriteRef.LoadToImage(icon);
			SetScale(1);
			TeleportToNewPlace();
		}

		private void SetScale(float scale)
		{
			transform.localScale = Vector3.one * scale;
		}

		private void TeleportToNewPlace()
		{
			var selfRT = (RectTransform) transform;
			var parentRT = (RectTransform) transform.parent;

			var parentHalfWidth = parentRT.rect.width / 2;
			var parentHalfHeight = parentRT.rect.height / 2;

			var halfWidth = selfRT.rect.width / 2;
			var halfHeight = selfRT.rect.width / 2;

			// set position
			selfRT.anchoredPosition = new Vector2(
				Random.Range(-parentHalfWidth + halfWidth, parentHalfWidth - halfWidth),
				Random.Range(-parentHalfHeight + halfHeight, parentHalfHeight - halfHeight)
			);
		}

		public void Hit()
		{
			if (!BonusManager.instance.IsBonusActive(BonusType.FixHuntObject))
			{
				TeleportToNewPlace();
			}

			int progressToAdd = BonusManager.instance.IsBonusActive(BonusType.DoublePress) ? 2 : 1;
			
			PageGame.instance.AddProgress(progressToAdd);
		}
	}
}