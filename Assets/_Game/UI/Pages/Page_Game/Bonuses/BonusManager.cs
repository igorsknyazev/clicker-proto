using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using _Game.Tools;
using UnityEngine;

namespace _Game.UI.Pages.Page_Game.Bonuses
{
	public class BonusManager : BaseSingletonBehaviour<BonusManager>
	{
		public Action<BonusType> OnBonusActivate;
		public Action<BonusType> OnBonusFinished;
		
		public float offerBonusPeriod = 3.5f;
		public List<Bonus> availableBonuses;

		private List<Bonus> shownBonuses = new List<Bonus>();

		public void StartBonusSpawn()
		{
			StartCoroutine(SpawnCoroutine());
		}

		private IEnumerator SpawnCoroutine()
		{
			while (true)
			{
				yield return new WaitForSeconds(offerBonusPeriod);
				
				// Find bonus which isn't spawned yet

				var availableToSpawn = availableBonuses.Where(bonus =>
					!shownBonuses.Exists(shownBonus => shownBonus.bonusType == bonus.bonusType)).ToList();

				if (availableToSpawn.Count == 0)
				{
					continue;
				}

				var luckyBonus = availableToSpawn.GetRandomElement();

				var spawnedBonus = Instantiate(luckyBonus, transform);
				shownBonuses.Add(spawnedBonus);
				spawnedBonus.Init((bonus) =>
				{
					OnBonusFinished?.Invoke(bonus.bonusType);
					shownBonuses.Remove(bonus);
					Destroy(bonus.gameObject);
				}, OnBonusActivate);
			}
		}

		public void StopBonusSpawn()
		{
			StopAllCoroutines();
		}

		public bool IsBonusActive(BonusType bonusType)
		{
			return shownBonuses.Any(bonus => bonus.bonusType == bonusType && bonus.IsBonusActive);
		}
	}
}