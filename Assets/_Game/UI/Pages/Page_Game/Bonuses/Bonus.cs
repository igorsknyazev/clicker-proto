using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace _Game.UI.Pages.Page_Game.Bonuses
{
	public enum BonusType
	{
		DoublePress,
		FixHuntObject,
		BiggerHuntObject
	}
	
	public class Bonus : MonoBehaviour
	{
		public BonusType bonusType;
		public float showTime = 3f;
		public float activeTime = 5f;

		[Header("UI elements")]
		public Image imageProgress;
		public Color colorOffer = Color.cyan;
		public Color colorActive = Color.red;

		private Action<Bonus> _disappearCallback;
		private Action<BonusType> _activateCallback;
		public bool IsBonusActive { get; private set; }
		
		public void Init(Action<Bonus> disappearCallback, Action<BonusType> activateCallback)
		{
			IsBonusActive = false;
			_disappearCallback = disappearCallback;
			_activateCallback = activateCallback;

			StartCoroutine(OfferBonus());
		}

		private float _timeOfferLeft;
		private IEnumerator OfferBonus()
		{
			imageProgress.color = colorOffer;
			_timeOfferLeft = showTime;

			while (_timeOfferLeft >= 0)
			{
				imageProgress.fillAmount = _timeOfferLeft / showTime;
				_timeOfferLeft -= Time.deltaTime;
				yield return null;
			}
			
			_disappearCallback?.Invoke(this);
		}

		public void Activate()
		{
			if (IsBonusActive)
			{
				return;
			}

			StopAllCoroutines();
			StartCoroutine(ActiveTimer());
		}

		private float _timeActiveLeft;
		private IEnumerator ActiveTimer()
		{
			_activateCallback?.Invoke(bonusType);

			imageProgress.color = colorActive;
			IsBonusActive = true;
			_timeActiveLeft = activeTime;

			while (_timeActiveLeft >= 0)
			{
				imageProgress.fillAmount = _timeActiveLeft / activeTime;
				_timeActiveLeft -= Time.deltaTime;
				yield return null;
			}
			
			_disappearCallback?.Invoke(this);
		}
	}
}