using UnityEngine;

namespace _Game.UI.Pages
{
	public class Page : MonoBehaviour
	{
		public bool isVisibleOnStart;
		
		private RectTransform _rectTransform;
		public RectTransform rectTransform => _rectTransform ??= GetComponent<RectTransform>();
		
		private void Awake()
		{
			if (isVisibleOnStart)
			{
				PlaceToViewport();
			}
			else
			{
				PlaceToHiddenChest();
			}
		}

		public void PlaceToViewport()
		{
			rectTransform.offsetMax = new Vector2(0, rectTransform.offsetMax.y);
			rectTransform.offsetMin = new Vector2(0, rectTransform.offsetMin.y);
		}

		public void PlaceToHiddenChest()
		{
			rectTransform.offsetMax = new Vector2(3000, rectTransform.offsetMax.y);
			rectTransform.offsetMin = new Vector2(3000, rectTransform.offsetMin.y);
		}

		public void EnablePage()
		{
			PlaceToViewport();
		}

		public void DisablePage()
		{
			PlaceToHiddenChest();
		}
	}
}