using System;
using _Game.Services;
using UnityEngine;

namespace _Game.UI.Pages.Page_Levels
{
	public class PageLevels : MonoBehaviour
	{
		public LevelUIItem prefabLevelItem;
		public RectTransform spawnBox;

		private void Start()
		{
			SpawnLevelItems();
		}

		private void SpawnLevelItems()
		{
			foreach (var levelKV in AssetsService.instance.levels)
			{
				var levelUIItem = Instantiate(prefabLevelItem, spawnBox);
				levelUIItem.Init(levelKV.Value);
			}
		}
	}
}