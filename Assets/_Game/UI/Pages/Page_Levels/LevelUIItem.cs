using _Game.Data;
using _Game.Tools;
using _Game.UI.Common.StarsRating;
using _Game.UI.Pages.Page_Game;
using _Game.UI.Popups.Popup_LevelInfo;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Game.UI.Pages.Page_Levels
{
	public class LevelUIItem : MonoBehaviour
	{
		public TextMeshProUGUI textName;
		public ImageLevelBackground imageLevelBackground;
		public StarsRating starsRating;

		private Level _level;

		public void Init(Level level)
		{
			_level = level;
			
			textName.text = level.levelName;
			
			imageLevelBackground.Init(level.backgroundImage);

			starsRating.Init(level);
		}

		public void ShowLevelDescription()
		{
			PopupLevelInfo.instance.Show(_level);
		}
	}
}