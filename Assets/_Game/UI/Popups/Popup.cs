using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using _Game.Tools;
using DG.Tweening;
using UnityEngine;

namespace _Game.UI.Popups
{
	[RequireComponent(typeof(UIHideableCanvasGroup))]
	public class Popup : MonoBehaviour
	{
		public List<DOTweenAnimation> appearAnimations;
		
		private UIHideableCanvasGroup _hideableCanvasGroup;
		private UIHideableCanvasGroup CGroup => _hideableCanvasGroup ??= GetComponent<UIHideableCanvasGroup>();
		
		public void Appear()
		{
			CGroup.SetVisible(true);
			foreach (var tweenAnimation in appearAnimations)
			{
				tweenAnimation.DOPlayForward();
			}
		}

		public async void Disappear()
		{
			var disappearTime = appearAnimations.Max(tween => tween.duration);
			foreach (var tweenAnimation in appearAnimations)
			{
				tweenAnimation.DOPlayBackwards();
			}

			await Task.Delay(TimeSpan.FromSeconds(disappearTime));
			
			CGroup.SetVisible(false);
		}
	}
}