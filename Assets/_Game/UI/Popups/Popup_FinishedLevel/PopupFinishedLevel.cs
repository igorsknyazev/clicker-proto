using _Game.Data;
using _Game.Tools;
using _Game.UI.Common.Leaderboard;
using _Game.UI.Pages;
using TMPro;
using UnityEngine;

namespace _Game.UI.Popups.Popup_FinishedLevel
{
	[RequireComponent(typeof(Popup))]
	public class PopupFinishedLevel : BaseSingletonBehaviour<PopupFinishedLevel>
	{
		public LeaderboardUI leaderboardUI;
		public TextMeshProUGUI textTime;
		
		private Popup _popup;
		private Popup Popup => _popup ??= GetComponent<Popup>();
		
		public void Show(Level level, float timeResult)
		{
			textTime.text = $"Your time: {timeResult:N1}";
			leaderboardUI.Init(level);
			Popup.Appear();
		}

		public void Close()
		{
			Popup.Disappear();
			PageNavigator.instance.Pop();
		}
	}
}