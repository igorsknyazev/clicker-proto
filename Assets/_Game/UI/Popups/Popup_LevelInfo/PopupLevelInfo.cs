using System.Collections.Generic;
using System.Linq;
using _Game.Data;
using _Game.Services.Storage;
using _Game.Services.Storage.Model;
using _Game.Tools;
using _Game.UI.Common.Leaderboard;
using _Game.UI.Common.StarsRating;
using _Game.UI.Pages;
using _Game.UI.Pages.Page_Game;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Game.UI.Popups.Popup_LevelInfo
{
	[RequireComponent(typeof(Popup))]
	public class PopupLevelInfo : BaseSingletonBehaviour<PopupLevelInfo>
	{
		public ImageLevelBackground imageLevelBackground;
		public TextMeshProUGUI textLevelName;
		public StarsRating starsRating;
		public LeaderboardUI leaderboardUI;
		
		private Popup _popup;
		private Popup Popup => _popup ??= GetComponent<Popup>();

		private Level _level;
		
		public void Show(Level level)
		{
			_level = level;

			imageLevelBackground.Init(_level.backgroundImage);
			textLevelName.text = _level.levelName;
			starsRating.Init(_level);
			leaderboardUI.Init(_level);
			
			Popup.Appear();
		}

		public void StartLevel()
		{
			Popup.Disappear();
			PageNavigator.instance.Push(PageNavigator.instance.pageGame);
			PageGame.instance.StartGame(_level);
		}
	}
}