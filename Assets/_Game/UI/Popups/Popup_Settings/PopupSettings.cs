using _Game.Tools;
using UnityEngine;

namespace _Game.UI.Popups.Popup_Settings
{
	[RequireComponent(typeof(Popup))]
	public class PopupSettings : BaseSingletonBehaviour<PopupSettings>
	{
		private Popup _popup;
		private Popup Popup => _popup ??= GetComponent<Popup>();
		
		public void Show()
		{
			Popup.Appear();
		}
	}
}