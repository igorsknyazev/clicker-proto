using _Game.Services;
using _Game.Services.Storage;
using _Game.Tools;
using TMPro;
using UnityEngine;

namespace _Game.UI.Popups.Popup_Settings
{
	public class UISettingPlayerName : MonoBehaviour
	{
		public TextMeshProUGUI textPlayerName;

		private void Start()
		{
			StorageService.instance.Settings.OnPlayerNameChanged += Redraw;
			Redraw(StorageService.instance.Settings.PlayerName);
		}

		private void Redraw(string name)
		{
			textPlayerName.text = name;
		}

		public void UpdateName()
		{
			StorageService.instance.Settings.PlayerName = AssetsService.instance.playerNames.GetRandomElement();
		}
	}
}