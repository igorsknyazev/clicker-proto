using _Game.Services.Storage;
using TMPro;
using UnityEngine;

namespace _Game.UI.Popups.Popup_Settings
{
	public class UISwitchSound : MonoBehaviour
	{
		public TextMeshProUGUI textState;
		
		private void Start()
		{
			StorageService.instance.Settings.OnSoundChanged += Redraw;
			Redraw(StorageService.instance.Settings.HasSound);
		}

		private void Redraw(bool isEnabled)
		{
			textState.text = isEnabled ? "On" : "Off";
		}

		public void ChangeState()
		{
			StorageService.instance.Settings.HasSound = !StorageService.instance.Settings.HasSound;
		}
	}
}