using _Game.Services.Storage;
using TMPro;
using UnityEngine;

namespace _Game.UI.Popups.Popup_Settings
{
	public class UISwitchMusic : MonoBehaviour
	{
		public TextMeshProUGUI textState;
		
		private void Start()
		{
			StorageService.instance.Settings.OnMusicChanged += Redraw;
			Redraw(StorageService.instance.Settings.HasMusic);
		}

		private void Redraw(bool isEnabled)
		{
			textState.text = isEnabled ? "On" : "Off";
		}

		public void ChangeState()
		{
			StorageService.instance.Settings.HasMusic = !StorageService.instance.Settings.HasMusic;
		}
	}
}