using UnityEngine;
using UnityEngine.AddressableAssets;

namespace _Game.Data
{
	[CreateAssetMenu(fileName = "Level_", menuName = "Level scriptable", order = 0)]
	public class Level : ScriptableObject
	{
		public string levelName;
		public AssetReferenceSprite backgroundImage;
		public AssetReferenceSprite targetImage;
	}
}