using System;
using System.IO;
using UnityEngine;

namespace _Game
{
	public static class Config
	{
		public static readonly String SAVE_FOLDER = Path.Combine(Application.persistentDataPath, "userData");
		public static readonly String GAME_FILE_SAVE_PROGRESS = "progress.dat";
		public static readonly String GAME_FILE_SAVE_SETTINGS = "settings.dat";

		public static readonly String DEMO_DATA_URI = "https://api.npoint.io/013d8e4dfd4ab3d73b57";
	}
}